import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class UndoableProgram {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Invoker invoker = new Invoker();

        invoker.execute(new AddCommand(list, 10));
        invoker.execute(new AddCommand(list, 20));
        System.out.println(list); // [10, 20]

        invoker.undo();
        System.out.println(list); // [10]
    }

    public static interface Command {

        void execute();

        void undo();
    }

    public static class AddCommand implements Command {

        private final List<Integer> list;
        private final int value;

        public AddCommand(List<Integer> list, int value) {
            this.list = list;
            this.value = value;
        }

        public void execute() {
            list.add(value);
        }

        public void undo() {
            list.remove(list.size() - 1);
        }
    }

    public static class Invoker {

        private final Stack<Command> commands = new Stack<>();

        public void execute(Command command) {
            command.execute();
            commands.push(command);
        }

        public void undo() {
            if (!commands.isEmpty()) {
                Command command = commands.pop();
                command.undo();
            }
        }
    }
}
