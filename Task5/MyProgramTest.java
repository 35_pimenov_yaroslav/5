import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;

public class MyProgramTest {

    @Test
    public void testFunction1() {
        MyProgram program = new MyProgram();
        int result = program.function1(2, 3);
        assertEquals(5, result);
    }

    @Test
    public void testFunction2() {
        MyProgram program = new MyProgram();
        String result = program.function2("hello");
        assertEquals("HELLO", result);
    }

    @Test
    public void testFunction3() {
        MyProgram program = new MyProgram();
        boolean result = program.function3(true);
        assertFalse(result);
    }

    private static class MyProgram {

        public MyProgram() {
        }

        private String function2(String hello) {
            throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        private int function1(int i, int i0) {
            throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }

        private boolean function3(boolean b) {
            throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        }
    }
}
