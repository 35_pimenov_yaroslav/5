Завдання виконав: Піменов Ярослав<br>
Тема: Обробка колекцій. Шаблони Singletone, Command<br>
Виконання завдання:<br>
Завдання 1:<br>
![Alt text](/image_2023-03-21_18-31-25.png "Optional title")<br>
![Alt text](/image_2023-03-21_18-30-58.png "Optional title")<br>
![Alt text](/image_2023-03-21_18-31-31.png "Optional title")<br>
Завдання 2:<br>
![Alt text](/photo_2023-03-21_18-37-19.jpg "Optional title")<br>
![Alt text](/photo_2023-03-21_18-37-24.jpg "Optional title")<br>
![Alt text](/photo_2023-03-21_18-37-29.jpg "Optional title")<br>
Завдання 3:<br>
![Alt text](/image_2023-03-21_18-41-31.png "Optional title")<br>
Завдання 4:<br>
![Alt text](/image_2023-03-21_18-43-07.png "Optional title")<br>
Завдання 5:<br>
![Alt text](/image_2023-03-21_18-44-18.png "Optional title")<br>
![Alt text](/image_2023-03-21_18-44-27.png "Optional title")<br>
