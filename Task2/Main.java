import java.util.ArrayList;
import java.util.List;

// інтерфейс для команд
interface Command {
void execute();
}

// клас для реалізації операції
class Operation {
public void perform(String message) {
System.out.println("Performing operation with message: " + message);
}
}

// клас для реалізації команди
class OperationCommand implements Command {
private Operation operation;
private String message;

public OperationCommand(Operation operation, String message) {
this.operation = operation;
this.message = message;
}

public void execute() {
operation.perform(message);
}
}

// клас для макрокоманди
class MacroCommand implements Command {
private List<Command> commands = new ArrayList<>();

public void add(Command command) {
commands.add(command);
}

public void execute() {
for (Command command : commands) {
command.execute();
}
}
}

// демонстрація роботи макрокоманди
public class Main {
public static void main(String[] args) {
Operation operation = new Operation();
Command command1 = new OperationCommand(operation, "Hello");
Command command2 = new OperationCommand(operation, "World");

MacroCommand macroCommand = new MacroCommand();
macroCommand.add(command1);
macroCommand.add(command2);

macroCommand.execute();
}
}   

