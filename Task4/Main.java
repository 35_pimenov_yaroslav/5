import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Привіт! Яка твоя назва?");
        String name = scanner.nextLine();
        
        System.out.println("Привіт, " + name + "! Яка твоя улюблена мова програмування?");
        String language = scanner.nextLine();
        
        System.out.println("Цікаво! Я теж люблю програмувати на " + language + ". До побачення!");
    }
}
